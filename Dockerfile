FROM alpine:latest AS build
RUN apk update \
  && apk upgrade \
  && apk add git npm 
RUN mkdir /build
COPY . /build
WORKDIR /build
RUN npm install \
  && npm run all

FROM alpine:latest
RUN mkdir /app
COPY --from=build /build/dist /app
RUN apk update \
  && apk upgrade \
  && apk add darkhttpd
EXPOSE 8000/tcp
CMD ["darkhttpd", "/app", "--port", "8000"]
