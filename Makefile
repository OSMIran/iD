UPSTREAM=https://github.com/openstreetmap/iD

sync:
	@git remote -v | grep upstream || git remote add upstream $(UPSTREAM)
	@git fetch upstream
	@git merge upstream/gh-pages
